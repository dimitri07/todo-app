const MESES = [
  "Enero",
  "Febrero",
  "Marzo",
  "Abril",
  "Mayo",
  "Junio",
  "Julio",
  "Agosto",
  "Septiembre",
  "Octubre",
  "Noviembre",
  "Diciembre",
];

const DIAS_SEMANAS = [
  "Lunes",
  "Martes",
  "Miércoles",
  "Jueves",
  "Viernes",
  "Sábado",
  "Domingo"
];

const dateManager = {
  name: 'DateManager',
  converMonthToString(idMonth) {
    return MESES[idMonth]
  },
  converDayWeekToString(idDayWeek) {
    return DIAS_SEMANAS[idDayWeek - 1]
  }
}

export default ({app}, inject) => {
  inject('dateManager', dateManager)
}
